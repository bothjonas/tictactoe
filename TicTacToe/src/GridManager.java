import java.util.Scanner;

public class GridManager {
	private Scanner sc = new Scanner(System.in);
	private int[][] grid = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

	public void print() {
		for(int i = 0; i < grid.length;i++) {
			for(int j = 0; j < grid[i].length;j++) {
				if( j <= 1) {
					System.out.print(grid[i][j]);
				}else {
					System.out.println(grid[i][j]);
				}
			}
		}
	}

	public void chooseBox() {
		
		int rowSelection = getInputFromUser("choose the row, where you want your tick(1-3).");
		int colSelection = getInputFromUser("choose the col, where you want your tick(1-3).");
		
		grid[rowSelection][colSelection] = 1; // user 1
	}

	private int getInputFromUser(String question) {
		int tempInput;
		boolean wrongInput;
		
		do {
		System.out.println(question);
		tempInput = sc.nextInt();
		if (tempInput <= 3 && tempInput >= 1) {
			System.out.println("You chose " + tempInput);
			wrongInput = false;
		} else {
			wrongInput = true;
			System.out.println("please choose a number between 1 and 3");
		}
		}while(wrongInput == true);
		return tempInput;
	
		}}
